#pragma once

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace mr {
namespace vision {

class Camera {
public:
	Camera(unsigned index);
	~Camera();

	void update();
	void close();

	const cv::Mat&getFrame() const;
private:
	void updateFrame();

	cv::Mat frame;

	cv::VideoCapture videoCapture;
};

} // vision
} // mr