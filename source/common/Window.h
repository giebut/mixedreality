#pragma once

#include <string>

namespace mr {
namespace common {

class Window {
public:
	Window(const std::string&name);
	~Window();
private:
	int height;
	int width;
};

} // common
} // mr