#include "Camera.h"

namespace mr {
namespace vision {

Camera::Camera(unsigned index) :
	videoCapture(index)
{
	while (frame.empty() && videoCapture.isOpened()) {
		videoCapture >> frame;
	}
}

Camera::~Camera() {}

void Camera::update() {
	updateFrame();
}

void Camera::close(){
	frame.release();
	videoCapture.release();
}

const cv::Mat & Camera::getFrame() const {
	return frame;
}

void Camera::updateFrame() {
	frame.release();
	while (frame.empty() && videoCapture.isOpened()) {
		videoCapture >> frame;
	}
}

} // vision
} // mr