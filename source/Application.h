#pragma once

#include <memory>

#include "common/Window.h"
#include "display/Canvas.h"
#include "vision/Camera.h"

namespace mr {

class Application {
public:
	static void run(int*argc, char**argv);
	static int destroy();
	~Application();
private:
	static void onDisplayFunc();
	static void onIdleFunc();
	static void onKeyboardFunc(unsigned char key, int x, int y);
	static void onMouseFunc(int button, int state, int x, int y);

	static int glutWindowId;
	static Application*application;

	Application();
	Application(const Application&) = delete;
	const Application&operator=(const Application&) = delete;

	void onDisplay();
	void onIdle();
	void onKeyboard(unsigned char key, int x, int y);
	void onMouse(int button, int state, int x, int y);

	std::unique_ptr<common::Window> window;
	std::unique_ptr<display::Canvas> canvas;
	std::unique_ptr<vision::Camera> camera;
};

} // mr