#include "Application.h"

#include <GL/freeglut.h>

namespace mr {

namespace {
const auto applicationName = "MixedReality";
}

// STATIC PUBLIC //

void Application::run(int * argc, char ** argv) {
	if (application == nullptr) {
		glutInit(argc, argv);
		glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_ALPHA);

		glutWindowId = glutCreateWindow(applicationName);

		glutDisplayFunc(onDisplayFunc);
		glutIdleFunc(onIdleFunc);
		glutKeyboardFunc(onKeyboardFunc);
		glutMouseFunc(onMouseFunc);

		application = new Application();

		glutMainLoop();
		printf("run\n");
	}
}

int Application::destroy(){
	printf("destroy");
	glutDestroyWindow(glutWindowId);

	delete application;
	application = nullptr;

	return 0;
}

// PUBLIC //

Application::~Application() {
	canvas.release();
	camera.release();
	window.release();
}

// STATIC PRIVATE //

int Application::glutWindowId = 0;
Application*Application::application = nullptr;

void Application::onDisplayFunc(){
	glutSwapBuffers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	application->onDisplay();
}

void Application::onIdleFunc(){
	application->onIdle();

	glutPostRedisplay();
}

void Application::onKeyboardFunc(unsigned char key, int x, int y){
	application->onKeyboard(key, x, y);
}

void Application::onMouseFunc(int button, int state, int x, int y){
	application->onMouse(button, state, x, y);
}

// PRIVATE //

Application::Application() {
	window = std::make_unique<common::Window>(applicationName);
	camera = std::make_unique<vision::Camera>(0);
	canvas = std::make_unique<display::Canvas>(camera->getFrame());
}

void Application::onDisplay(){
	canvas->draw();
}

void Application::onIdle(){
	camera->update();
}

void Application::onKeyboard(unsigned char key, int x, int y){
	if (key == 27) {
		camera->close();
		glutLeaveMainLoop();
	}
}

void Application::onMouse(int button, int state, int x, int y){
}

} // mr