#include "Canvas.h"

namespace mr {
namespace display {

Canvas::Canvas(const cv::Mat & background) :
	background(background)
{
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glGenTextures(1, &textureId);

	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

Canvas::~Canvas(){}

void Canvas::draw(){
	if (!background.empty()) {
		drawBackground();
	}
}

void Canvas::drawBackground(){
	GLint polygonMode[2];

	glGetIntegerv(GL_POLYGON_MODE, polygonMode);
	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_BACK, GL_FILL);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 1.0, 0.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, background.size().width, background.size().height, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, background.ptr());

	glEnable(GL_TEXTURE_2D);
	glBegin(GL_TRIANGLES);
	glNormal3d(0.0, 0.0, 1.0);

	glTexCoord2d(0.0, 1.0);
	glVertex3d(0.0, 0.0, 0.0);
	glTexCoord2d(0.0, 0.0);
	glVertex3d(0.0, 1.0, 0.0);
	glTexCoord2d(1.0, 1.0);
	glVertex3d(1.0, 0.0, 0.0);

	glTexCoord2d(1.0, 1.0);
	glVertex3d(1.0, 0.0, 0.0);
	glTexCoord2d(0.0, 0.0);
	glVertex3d(0.0, 1.0, 0.0);
	glTexCoord2d(1.0, 0.0);
	glVertex3d(1.0, 1.0, 0.0);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glClear(GL_DEPTH_BUFFER_BIT);

	glPolygonMode(GL_FRONT, polygonMode[0]);
	glPolygonMode(GL_BACK, polygonMode[1]);
}

}
}