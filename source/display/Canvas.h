#pragma once

#include <opencv2/imgproc/imgproc.hpp>

#include <GL/freeglut.h>

namespace mr {
namespace display {

class Canvas {
public:
	Canvas(const cv::Mat&background);
	~Canvas();

	void draw();
private:
	void drawBackground();

	GLuint textureId;

	const cv::Mat&background;
};

} // display
} // mr