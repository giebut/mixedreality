#include "Application.h"

int main(int argc, char*argv) {
	mr::Application::run(&argc, &argv);
	return mr::Application::destroy();
}