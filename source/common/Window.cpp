#include "Window.h"

#include <vector>

#include <Windows.h>

namespace mr {
namespace common {

namespace {
BOOL CALLBACK monitorEnumProc(HMONITOR hMonitor, HDC hdc, LPRECT lpRect, LPARAM lParam) {
	reinterpret_cast<std::vector<HMONITOR>*>(lParam)->push_back(hMonitor);

	return TRUE;
}

RECT getMonitorRect(int index) {
	std::vector<HMONITOR> monitors;
	EnumDisplayMonitors(nullptr, nullptr, monitorEnumProc, reinterpret_cast<LPARAM>(&monitors));

	MONITORINFO mi;

	mi.cbSize = sizeof(mi);
	GetMonitorInfo(monitors[index], &mi);

	return mi.rcMonitor;
}

void setWindow(const std::string&name, int x, int y, int width, int height) {
	HWND window = FindWindow(0, name.c_str());

	long windowLong = GetWindowLong(window, GWL_EXSTYLE) | WS_EX_TOPMOST;
	int flags = (SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOZORDER) & ~SWP_NOSIZE;

	SetWindowLong(window, GWL_STYLE, windowLong);
	SetWindowPos(window, HWND_TOPMOST, x, y, width, height, flags);

	ShowWindow(window, SW_SHOW);
}
}

Window::Window(const std::string&name) {
	const auto&rect = getMonitorRect(GetSystemMetrics(SM_CMONITORS) - 1);

	height = rect.bottom - rect.top;
	width = rect.right - rect.left;

	setWindow(name, rect.left, rect.top, width, height);
}

Window::~Window() {}

} // common
} // mr